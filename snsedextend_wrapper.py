# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)


def get_all_color_tables(arg_data_photometry_all, arg_dict_models_fit):
    """
    Create a cumulative color table from all photometry data and sncosmo models.
    :param arg_data_photometry_all: list of formatted photometric data series
    :param arg_dict_models_fit: dictionary of models and their metadata
    :return: astropy table with the cumulative color table
    """

    import snsedextend
    from snutils.model.sncosmo.metadata_extraction import get_parameters, get_parameter_bounds, get_vparams

    list_color_tables = []
    for loop_data_photometry in arg_data_photometry_all:
        loop_name, res_from_fit, loop_model = arg_dict_models_fit[loop_data_photometry["event"][0]]

        # only varied parameters (vparams)
        dict_vparams = get_vparams(res_from_fit)

        # only provided parameters
        dict_parameters = get_parameters(res_from_fit)

        # turn the vparams and their error margins into bounds
        dict_bounds = get_parameter_bounds(res_from_fit, dict_vparams)

        # make color table
        list_color_tables.append(
            snsedextend.curveToColor(loop_data_photometry, ["r-z"], snName=loop_name,
                                     bandDict={'g': "sdss::g",
                                               'r': "sdss::r",
                                               'i': "sdss::i",
                                               'z': "sdss::z"},
                                     model=loop_model._source.name,
                                     zpsys=loop_data_photometry["zpsys"][0], constants=dict_parameters,
                                     bounds=dict_bounds, dust="CCM89Dust", effect_frames=["rest", "obs"],
                                     effect_names=["host", "mw"]))

    combined_color_tables = snsedextend.colorTableCombine(list_color_tables)

    return combined_color_tables


def fit_all_color_tables(arg_color_tables_all, param_type):
    """
    Run Gaussian Process regression on a cumulative color table.
    :param arg_color_tables_all: a cumulative color table
    :return: a Gaussian Process Regression model of a cumulative color table
    """

    import snsedextend

    # TODO: make sure color table fitting works
    all_color_tables_fit = snsedextend.fitColorCurve(arg_color_tables_all, type=param_type)

    return all_color_tables_fit
