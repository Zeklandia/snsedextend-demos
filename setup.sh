#!/bin/bash
# Written by: Asher Wood (zeklandia)

# list of supernovae types
TYPES=('Ia' 'Ib' 'Ic' 'II L' 'IIn' 'II P')

LOCAL_ROOT_DIR=$PWD

# make a directory if it does not exist
chkdirmkdir(){
	if [ ! -d ./"${*}" ]
	then
		echo "🛠 .${PWD#$LOCAL_ROOT_DIR}/${*}..."
		mkdir ./"${*}"
	else
		echo "✔ .${PWD#$LOCAL_ROOT_DIR}/${*}"
	fi
}

# make input data directories for each supernova type
setuptypeinputdirs(){
	for type in "${TYPES[@]}"
	do
		chkdirmkdir "$type"
		
		# entering ./input/data/$type/
		cd ./"$type"
		
		chkdirmkdir "params"
		
		# entering ./input/data/$type/params
		cd ./params
		
		chkdirmkdir "bounds"
		chkdirmkdir "dust"
		
		cd ..
		# leaving ./input/data/$type/params
		
		chkdirmkdir "phot"
		chkdirmkdir "sed"
		
		cd ..
		# leaving ./input/data/$type/
	done
}

# make input directories
mkinputdirs(){
	chkdirmkdir "input"
	
	# entering ./input/
	cd ./input
	
	chkdirmkdir "data"
	
	# entering ./input/data/
	cd ./data
	setuptypeinputdirs
	
	cd ..
	# leaving ./input/data/
	
	chkdirmkdir "reference"
	chkdirmkdir "settings"
	
	cd ..
	# leaving ./input/
}

# make output data directories for each supernova type
setuptypeoutputdirs(){
	for type in "${TYPES[@]}"
	do
		chkdirmkdir "$type"
		
		# entering ./output/data/$type/
		cd ./"$type"

		chkdirmkdir "color"
		chkdirmkdir "phot"
		chkdirmkdir "plots"
		chkdirmkdir "sed"
		
		cd ..
		# leaving ./output/data/$type/
	done
}

# make output directories
mkoutputdirs(){
	chkdirmkdir "output"
	
	# entering ./output/
	cd ./output
	
	chkdirmkdir "cache"
	
	chkdirmkdir "data"
	
	# entering ./output/data/
	cd ./data
	
	setuptypeoutputdirs
	
	cd ..
	# leaving ./output/data/
	
	cd ..
	# leaving ./output/
}

# make input and output data directories
mkinputdirs
mkoutputdirs
