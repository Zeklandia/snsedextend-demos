# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

from pathlib import Path

import snsedextend

gen_sed = Path(Path.cwd() / "input" / "data" / "Ic" / "sed").glob("*.[Ss][Ee][Dd]")
list_sed = [lc_path for lc_path in gen_sed]

#######################
# BEGIN TUTORIAL CODE #
#######################

# load sample SED
sedFile = snsedextend.example_sed

# load sample lightcurve
myLC = snsedextend.load_example_lc()

# create color table
colorTable = snsedextend.curveToColor(myLC, colors=['U-B', 'r-J', 'r-H', 'r-K'], snName="test", snType='Ic',
                                      zpsys='vega', bounds={'hostebv': (-1, 1), 't0': (53787.94, 53797.94)},
                                      constants={'mwr_v': 3.1, 'mwebv': '0.1267', 'z': '0.033529863', 'hostr_v': 3.1},
                                      dust='CCM89Dust', effect_frames=['rest', 'obs'], effect_names=['host', 'mw'])

# fit color curves
curveDict = snsedextend.fitColorCurve(colorTable)

# create SED
# TODO: snType was not mentioned in the documentation core-collapse example
newSED = snsedextend.extendCC(colorTable, curveDict, snType="Ic",
                              sedlist=list_sed,
                              zpsys='vega', showplots=True, verbose=True)

# plot new SED
for sed in [path for path in Path.cwd().glob("*.[Ss][Ee][Dd]")]:
    snsedextend.plotSED(sed, day=1, showPlot=True, MINWAVE=4000, MAXWAVE=20000, saveFig=False)

#####################
# END TUTORIAL CODE #
#####################
