# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

from pathlib import Path
from sys import stderr

import numpy as np
import sncosmo
import snsedextend
from astropy.io import ascii
from tqdm import tqdm
from zutils.file.read.csv import csv_to_list, csv_to_dict
from zutils.file.read.txt import txt_line_to_str

#################################
# STATIC GLOBAL VARIABLES START #
#################################

# PATHS
# ./input/
PATH_DIR_INPUT = Path.cwd() / "input"

# ......./ctrl/
# ............/bands_sncosmo.csv
# ............/bands_snsedextend.csv
PATH_DIR_INPUT_CTRL = PATH_DIR_INPUT / "ctrl"
PATH_FILE_PREF_BANDS_FITTING = PATH_DIR_INPUT_CTRL / "bands_sncosmo.csv"
PATH_FILE_PREF_BANDS_COLOR_CURVE_FITTING = PATH_DIR_INPUT_CTRL / "bands_snsedextend.csv"
PATH_FILE_PREF_CULLING = PATH_DIR_INPUT_CTRL / "cull.txt"

# ......./input/fit/
PATH_DIR_INPUT_FIT = PATH_DIR_INPUT / "fit"

# ............/ref/
# ................/models.csv
PATH_DIR_INPUT_REF = PATH_DIR_INPUT / "ref"
PATH_FILE_REF_MODELS_SNCOSMO_SELECTED = PATH_DIR_INPUT_REF / "models.csv"

###############################
# STATIC GLOBAL VARIABLES END #
###############################

######################################
# NON- STATIC GLOBAL VARIABLES START #
######################################

# DATASET VARIABLES
# get list of bands to use for fitting
pref_bands_fitting = csv_to_list(PATH_FILE_PREF_BANDS_FITTING)

# get list of bands to use for color curve fitting
pref_bands_color_curve_fitting = csv_to_list(PATH_FILE_PREF_BANDS_COLOR_CURVE_FITTING)

# the number of days after the first data point to keep
pref_culling = int(txt_line_to_str(PATH_FILE_PREF_CULLING))

# import list of selected models and their types
ref_dict_models_sncosmo_ids_selected = csv_to_dict(PATH_FILE_REF_MODELS_SNCOSMO_SELECTED)


####################################
# NON- STATIC GLOBAL VARIABLES END #
####################################

###################
# FUNCTIONS START #
###################


def get_sncosmo_models(arg_sn_type):
    # get list of supported sncosmo models
    ref_array_sncosmo_models_found = np.unique(
        [lc_model[0] for lc_model in sncosmo.models._SOURCES._loaders.keys() if "snana" in lc_model[0]])

    # filter out models of the wrong type, remove salt2 because it needs amplitude and I ain't got it
    ref_dict_models_sncosmo_ids_selected_filtered = {lc_model: lc_model_type for lc_model, lc_model_type in
                                                     ref_dict_models_sncosmo_ids_selected.items() if
                                                     lc_model_type == arg_sn_type if lc_model != "salt2"}

    # a simple list of the sncosmo model names
    ref_list_models_sncosmo_ids_selected_filtered = list(ref_dict_models_sncosmo_ids_selected_filtered.keys())

    return ref_list_models_sncosmo_ids_selected_filtered


def get_paths_for_type(arg_sn_type):
    """
    File paths for data based on supernova type.
    TODO: finish documentation
    :param arg_sn_type:
    :return:
    """

    # ./input/fit/[type]/
    # ................../mwebv.csv
    # ................../z.csv
    PATH_DIR_INPUT_FIT_TYPE = PATH_DIR_INPUT_FIT / arg_sn_type
    PATH_CSV_PARAM_MWEBV = PATH_DIR_INPUT_FIT_TYPE / "mwebv.csv"
    PATH_CSV_PARAM_Z = PATH_DIR_INPUT_FIT_TYPE / "z.csv"
    PATH_CSV_MODELS_BEST = PATH_DIR_INPUT_FIT_TYPE / "best_models.csv"

    # ................../dust/
    # ......................./hostr_v.csv
    # ......................./mwr_v.csv
    PATH_DIR_INPUT_FIT_TYPE_DUST = PATH_DIR_INPUT_FIT_TYPE / "parameters"
    PATH_CSV_PARAM_DUST_HOSTR_V = PATH_DIR_INPUT_FIT_TYPE_DUST / "hostr_v.csv.csv"
    PATH_CSV_PARAM_DUST_MWR_V = PATH_DIR_INPUT_FIT_TYPE_DUST / "mwr_v.csv"

    # ................../params/
    # ........................./hostebv.csv
    # ........................./t0.csv
    PATH_DIR_INPUT_FIT_TYPE_PARAMS = PATH_DIR_INPUT_FIT_TYPE / "parameters"
    PATH_CSV_PARAM_HOSTEBV = PATH_DIR_INPUT_FIT_TYPE_PARAMS / "hostebv.csv"
    PATH_CSV_PARAM_T0 = PATH_DIR_INPUT_FIT_TYPE_PARAMS / "t0.csv"

    # ................../phot/
    PATH_DIR_INPUT_FIT_TYPE_PHOT = PATH_DIR_INPUT_FIT_TYPE / "photometry"

    # ................../sed/
    PATH_DIR_INPUT_FIT_TYPE_SED = PATH_DIR_INPUT_FIT_TYPE / "sed"

    dict_paths_for_type = {"mwebv.csv": PATH_CSV_PARAM_MWEBV,
                           "z.csv": PATH_CSV_PARAM_Z,
                           "hostebv.csv": PATH_CSV_PARAM_HOSTEBV,
                           "t0.csv": PATH_CSV_PARAM_T0,
                           "hostr_v.csv": PATH_CSV_PARAM_DUST_HOSTR_V,
                           "mwr_v.csv": PATH_CSV_PARAM_DUST_MWR_V,
                           "best_models.csv": PATH_CSV_MODELS_BEST,
                           "/phot": PATH_DIR_INPUT_FIT_TYPE_PHOT,
                           "/sed": PATH_DIR_INPUT_FIT_TYPE_SED}

    return dict_paths_for_type


def read_all_photometry_data_files(arg_path_data_photometry):
    """
    Read photometry data from CSV files in a directory into a list of data series.
    :param arg_path_data_photometry: directory to search for photometry data CSV files("sne_*.csv")
    :return: list of data series, each series is the data from a single CSV file
    """
    # find lightcurve files
    paths_data_photometry = arg_path_data_photometry.glob("[Ss][Nn][Ee]_*.[Cc][Ss][Vv]")

    # read data files into a list
    data_photometry_all = [ascii.read(lc_path) for lc_path in paths_data_photometry]

    return data_photometry_all


def load_all_sed_files(arg_paths_sed):
    """
    Get a list of all the SED files in a directory.
    :param arg_paths_sed: directory to search for SED files
    :return: list of SED file paths
    """
    # find SED files
    paths_data_photometry = arg_paths_sed.glob("*.[Ss][Ee][Dd]")

    # read data files into a list
    files_sed_all = [lc_path for lc_path in paths_data_photometry]

    return files_sed_all


def plot_all_models(arg_data_photometry_all_formatted, arg_dict_models_fit, arg_save_plot=False):
    """
    Plot all fit sncosmo models against their data.
    TODO: complete plot_all_models documentation
    :param arg_data_photometry_all_formatted: list of formatted photometric data series
    :param arg_dict_models_fit: dictionary of events and the models fit to them
    :param arg_save_plot
    """

    import matplotlib.pyplot as plt

    # use tqdm to track the loop's progress
    status_pbar_loop_plotting = tqdm(desc="Plotting sncosmo models", total=len(arg_dict_models_fit.keys()),
                                     unit=" plots")

    for loop_model_fit in arg_dict_models_fit:
        for loop_series in arg_data_photometry_all_formatted:
            # if the model
            if loop_model_fit == loop_series["event"][0]:
                # report which model is being plot
                status_pbar_loop_plotting.write("Plotting " + loop_model_fit + "...")

                # use sncosmo to plot the lightcurve
                sncosmo.plot_lc(loop_series, model=arg_dict_models_fit[loop_model_fit][2])
                plt.show()

                # save the plot
                if arg_save_plot:
                    # TODO: add plot saving to plot_all_models()
                    print("Plot saving not yet implemented!", file=stderr)
        # update the plotting progress bar
        status_pbar_loop_plotting.update(1)
    # close the progress plotting bar
    status_pbar_loop_plotting.close()


def full_fit(sn_type):
    """
    TODO: finish abstraction
    :param sn_type: supernova type
    :return:
    """

    from snutils.data.process import format_all_photometry_data_series, format_all_photometry_data_series_snsedextend
    from snutils.model.sncosmo.fit import get_all_best_fit_models
    from snsedextend_wrapper import get_all_color_tables

    # get type-specific paths
    dict_paths_for_type = get_paths_for_type(sn_type)

    # get the data
    data_photometry_all_raw = read_all_photometry_data_files(dict_paths_for_type["/phot"])

    # prepare data for sncosmo modelling
    data_photometry_all_formatted_sncosmo = format_all_photometry_data_series(data_photometry_all_raw,
                                                                              pref_bands_fitting,
                                                                              pref_culling)

    # include z band for snsedextend color curve
    data_photometry_all_formatted_snsedextend = format_all_photometry_data_series_snsedextend(data_photometry_all_raw,
                                                                                              pref_bands_color_curve_fitting,
                                                                                              pref_culling)

    # gather other data
    list_sncosmo_models = get_sncosmo_models(sn_type)

    # find the best models
    dict_ids_models_fit_best, dict_models_fit_best = get_all_best_fit_models(data_photometry_all_formatted_sncosmo,
                                                                             list_sncosmo_models,
                                                                             dict_paths_for_type["best_models.csv"])

    # see how well the models fit
    plot_all_models(data_photometry_all_formatted_sncosmo, dict_models_fit_best, arg_save_plot=True)

    # generate color curves from the data and all the best models
    color_tables_all = get_all_color_tables(data_photometry_all_formatted_snsedextend, dict_models_fit_best)

    dict_color_curve = snsedextend.fitColorCurve(color_tables_all)

    file_sed_new = snsedextend.extendCC(color_tables_all, dict_color_curve, sedlist=[load_all_sed_files()], zpsys="ab",
                                        showplots=True, verbose=True)

    return file_sed_new


#################
# FUNCTIONS END #
#################

#################
# CLASSES START #
#################


class SNSEDExtendBatch:
    def __init__(self):
        self.data = []


###############
# CLASSES END #
###############

#######################
# DRIVER SCRIPT START #
#######################

for loop_sn_type in ["ib", "ic"]:
    full_fit(loop_sn_type)

#####################
# DRIVER SCRIPT END #
#####################
