# -*- coding: utf-8 -*-
# Written by: Asher Wood (zeklandia)

from pathlib import Path

import matplotlib.pyplot as plt
import snsedextend
from astropy.io import ascii
from snutils.data.format import format_data_snsedextend

sn_type = "Ic"
sn_type_var = sn_type.replace(' ', '')

# get SED file paths
gen_sed = Path(Path.cwd() / "input" / "data" / sn_type / "sed").glob("*.[Ss][Ee][Dd]")
list_sed = [lc_path for lc_path in gen_sed]

# load sample lightcurve
myLC = format_data_snsedextend(ascii.read("./test/SN2013fs.csv"), ["g", "r", "z"], (56570.0, 56650.0))

# global variables
list_colors = ['r-z']

# create color table
colorTable = snsedextend.curveToColor(myLC, colors=list_colors, snName="test", snType=sn_type_var,
                                      bandDict={'g': "sdss::g",
                                                'r': "sdss::r",
                                                'i': "sdss::i",
                                                'z': "sdss::z"},
                                      zpsys='vega',
                                      bounds={'hostebv': (-1, 1), 't0': (53787.94, 53797.94)},
                                      constants={'mwr_v': 3.1, 'mwebv': '0.1267', 'z': '0.033529863', 'hostr_v': 3.1},
                                      dust='CCM89Dust', effect_frames=['rest', 'obs'], effect_names=['host', 'mw'])


# plot color table
def mk_plt(ax, time_column, color_column, color_err_column, label):
    # plot the transmission rate of the bandpass
    ax.plot(time_column, color_column, label=label)

    # set error bars
    plt.errorbar(time_column, color_column, yerr=color_err_column, fmt='o')

    # label axes
    ax.set_xlabel("Time (Julian days)")
    ax.set_ylabel("Color difference")


def plt_colortable():
    # make figure
    figure = plt.figure(dpi=300)
    figure.set_size_inches(9, 6.5)

    # make full first row plot
    if len(list_colors) > 3:
        ax = figure.add_subplot(3, 1, 1)
    else:
        ax = figure.add_subplot(2, 1, 1)

    # plot full first row plot
    for color in list_colors:
        mk_plt(ax, colorTable["time"], colorTable[color], colorTable[color.replace('-', '') + "_err"], color)

    # add title to first row plot
    ax.set_title("Example Color Table")

    # show legend for top row plot
    ax.legend(loc="upper right")

    # second row, first plot is four because the first three plots are used for the main plot
    i = 4

    # add each color to its own plot in the second row
    for color in list_colors:
        # choose the plot
        if len(list_colors) > 3:
            axi = figure.add_subplot(3, 3, i, sharey=ax)
        else:
            axi = figure.add_subplot(2, 3, i, sharey=ax)
        axi.yaxis.label.set_visible(False)
        axi.xaxis.label.set_visible(False)

        # plot the bandpass and get the max value
        mk_plt(axi, colorTable["time"], colorTable[color], colorTable[color.replace('-', '') + "_err"], color)

        # set the title for the subplot
        axi.set_title(color)

        # increment the plot coordinates
        i += 1

    # complete
    plt.tight_layout()
    plt.show()
    plt.close(figure)


plt_colortable()

# fit color curves
curveDict = snsedextend.fitColorCurve(colorTable)

# create SED
# TODO: snType was not mentioned in the documentation core-collapse example
newSED = snsedextend.extendCC(colorTable, curveDict, snType=sn_type_var, sedlist=list_sed, zpsys='vega', showplots=True,
                              verbose=True)

# plot new SED
for sed in [path for path in Path.cwd().glob("*.[Ss][Ee][Dd]")]:
    snsedextend.plotSED(sed, day=1, showPlot=True, MINWAVE=4000, MAXWAVE=20000, saveFig=False)
